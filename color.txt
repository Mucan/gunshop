������� ������: background: rgb(192,192,192); /* Old browsers */
background: -moz-linear-gradient(top,  rgba(192,192,192,1) 0%, rgba(254,254,254,1) 100%, rgba(203,235,255,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(192,192,192,1)), color-stop(100%,rgba(254,254,254,1)), color-stop(100%,rgba(203,235,255,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(192,192,192,1) 0%,rgba(254,254,254,1) 100%,rgba(203,235,255,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(192,192,192,1) 0%,rgba(254,254,254,1) 100%,rgba(203,235,255,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(192,192,192,1) 0%,rgba(254,254,254,1) 100%,rgba(203,235,255,1) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(192,192,192,1) 0%,rgba(254,254,254,1) 100%,rgba(203,235,255,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c0c0c0', endColorstr='#cbebff',GradientType=0 ); /* IE6-9 */

������� ����: #7a0c0e;

����������� ����: background: rgb(251,251,251); /* Old browsers */
background: -moz-linear-gradient(top,  rgba(251,251,251,1) 0%, rgba(216,216,216,1) 50%, rgba(216,216,216,1) 51%, rgba(251,251,251,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(251,251,251,1)), color-stop(50%,rgba(216,216,216,1)), color-stop(51%,rgba(216,216,216,1)), color-stop(100%,rgba(251,251,251,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(251,251,251,1) 0%,rgba(216,216,216,1) 50%,rgba(216,216,216,1) 51%,rgba(251,251,251,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(251,251,251,1) 0%,rgba(216,216,216,1) 50%,rgba(216,216,216,1) 51%,rgba(251,251,251,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(251,251,251,1) 0%,rgba(216,216,216,1) 50%,rgba(216,216,216,1) 51%,rgba(251,251,251,1) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(251,251,251,1) 0%,rgba(216,216,216,1) 50%,rgba(216,216,216,1) 51%,rgba(251,251,251,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fbfbfb', endColorstr='#fbfbfb',GradientType=0 ); /* IE6-9 */

������: background: rgb(254,254,254); /* Old browsers */
background: -moz-linear-gradient(top,  rgba(254,254,254,1) 0%, rgba(207,207,207,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(254,254,254,1)), color-stop(100%,rgba(207,207,207,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(254,254,254,1) 0%,rgba(207,207,207,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(254,254,254,1) 0%,rgba(207,207,207,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(254,254,254,1) 0%,rgba(207,207,207,1) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(254,254,254,1) 0%,rgba(207,207,207,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fefefe', endColorstr='#cfcfcf',GradientType=0 ); /* IE6-9 */

������� ���: background: rgb(191,65,32); /* Old browsers */
background: -moz-linear-gradient(top,  rgba(191,65,32,1) 0%, rgba(115,34,17,1) 50%, rgba(115,34,17,1) 51%, rgba(191,65,32,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(191,65,32,1)), color-stop(50%,rgba(115,34,17,1)), color-stop(51%,rgba(115,34,17,1)), color-stop(100%,rgba(191,65,32,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(191,65,32,1) 0%,rgba(115,34,17,1) 50%,rgba(115,34,17,1) 51%,rgba(191,65,32,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(191,65,32,1) 0%,rgba(115,34,17,1) 50%,rgba(115,34,17,1) 51%,rgba(191,65,32,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(191,65,32,1) 0%,rgba(115,34,17,1) 50%,rgba(115,34,17,1) 51%,rgba(191,65,32,1) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(191,65,32,1) 0%,rgba(115,34,17,1) 50%,rgba(115,34,17,1) 51%,rgba(191,65,32,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bf4120', endColorstr='#bf4120',GradientType=0 ); /* IE6-9 */

����� �����: #919191;
����� �����: #e6e6e6;